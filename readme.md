# Personal suckless slstatus config.h

- [Info](#info)
- [WARNING](#warning)

# Info

- What is slstatus ?
    - slstatus is a status monitor for window managers that use WM_NAME or stdin to fill the status bar.
    - slstatus is programmed in C, so you need to compile the source everytime you make a change.
    - [more info](https://tools.suckless.org/slstatus/)
- This config.h display:
    - Internet speed.
    - CPU percentage and temperature.
    - RAM percentage and used.
    - "/" partition free space.
    - Mic muted.
    - Volume percentage.
    - Kernel version.
    - Battery percentage.
    - Datetime.

[//]: # (![slstatus](slstatus.png?raw=true))

# WARNING

- Internet speed is set in 7 and 8. The arguments are the name of the network interface. Replace the network device name with the correct name for your device "enp7s0f3u1u2" -> "yourDeviceName".
    - You can list your network devices on you computer by running the following command:
        ```sh
        ip link
        ```
    - If you want to know which device is the one connected, you can run the following command:
        ```sh
        ip address | grep "state UP"
        ```
- CPU Temp (line 12). As an argument, it needs the temp file path.
- Volume % (line 15). Volume percentage requires pactl.
- Battery percentage requires device asigned name as arguments, replace "BAT0".
