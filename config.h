const unsigned int interval = 1000;
static const char unknown_str[] = "n/a";
#define MAXLEN 2048

static const struct arg args[] = {
	/* function format          argument */
	{ netspeed_tx, " ▲ %s -", "enp7s0f3u1u2" },
	{ netspeed_rx, " ▼ %s |", "enp7s0f3u1u2" },
	{ cpu_perc, "  %s%% |", NULL },
    { ram_perc, "  %s%% -", NULL },
	{ ram_used, " %s |", NULL },
    { temp, "  %s°C |", "/sys/class/thermal/thermal_zone0/temp" },
	{ disk_free, "  %s |", "/" },
	{ run_command, "  %s |", "pactl get-source-mute @DEFAULT_SOURCE@" },
	{ run_command, " 墳 %s |", "pactl get-sink-volume @DEFAULT_SINK@ | cut -d '/' -f 2" },
    { kernel_release, "  %s%% |", NULL },
	{ battery_perc, "  %s%% |", "BAT0" },
	{ datetime, "  %s", "%F %T" },
};
